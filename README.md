# Shinobi Video Exporter (customAutoLoad)

This is a customAutoLoad module allows promethous to collect metrics from Shinobi Video NVR based on existing events,
New endpoint will be exposed under - <http://domain/metrics>

## How to use

Custom Auto Load Modules (customAutoLoad Modules) are a functions sets that don't exist in the official source code but runs as if it does.

### Download

#### Manually

1. Open your Shinobi folder. By default this is /home/Shinobi .
2. Open the customAutoLoad directory
3. Download repository as ZIP file and unzip its content

#### Super dashboard

1. Open Shinobi Vide Super dashboard
2. Get into Custom Auto Load tab
3. Set the link to download the zip file of this repostiroy
4. Click on download

### Install

Once downloaded and appears in the list
![customAutoLoad](./docs/customAutoLoad.PNG "customAutoLoad")

1. Click on `Install`
2. Click on `Enable`
3. Restart Shinobi Video

### Add configuration to Promethous to start collect metrics

```yaml
scrape_configs:
  - job_name: 'shinobi'
    scrape_interval: 1s
    honor_labels: true
    static_configs:
      - targets: 
          - "hostname:port"
```

### Grafana

Import Grafana dashboard [JSON](./grafana.json)

### Endpoints

| Event              | Description                                    |
|--------------------|------------------------------------------------|
| /metrics           | Reterives collected metrics for Promethous     |
| /metrics/grafana   | Reterives Grafana dashboard to import          |


## Supported events

| Event              | Description                                    |
|--------------------|------------------------------------------------|
| os                 | Monitors system parameters                     |
| diskUsed           | Monitors disk usage parameters                 |
| monitor_status     | Monitors status of each camera when changed    |
| viewer_count       | Monitors viewers counter                       |
| detector_trigger   | Monitors detected events from all plugins      |

## Metrics

Metric name will be created based on the following pattern:
shinobi_{event_name}_{metric_key}

Instance will appear in all the metric's label, default value is *`shinobi`*,
its purpose is to allow collecting within the same promethous from multiple instances of Shinobi Video NVR.

In the exmaples below, instance set to *`shinobi_test`*

### OS

*Monitors system parameters*

Event name: **os**

Metric name: **os**

```ini
# HELP shinobi_os_cpu CPU Usage %
# TYPE shinobi_os_cpu gauge
shinobi_os_cpu instance="shinobi_test"} 34

# HELP shinobi_os_ram_used RAM Used MB
# TYPE shinobi_os_ram_used gauge
shinobi_os_ram_used {instance="shinobi_test"} 95356

# HELP shinobi_os_ram_used_percent RAM Usage %
# TYPE shinobi_os_ram_used_percent gauge
shinobi_os_ram_used_percent {instance="shinobi_test"} 33
```

### diskUsed

*Monitors disk space*

Event name: **diskUsed**

Metric name: **disk_used**

```ini
# HELP shinobi_disk_used_used_space_videos Used disk space in MB for videos
# TYPE shinobi_disk_used_used_space_videos gauge
shinobi_disk_used_used_space_videos {instance="shinobi_test"} 18575.777114868164

# HELP shinobi_disk_used_used_space_file_bin Used disk space in MB for File Bin
# TYPE shinobi_disk_used_used_space_file_bin gauge
shinobi_disk_used_used_space_file_bin {instance="shinobi_test"} 21.019458770751953

# HELP shinobi_disk_used_used_space_time_lapse_frames Used disk space in MB for Time Lapse Frames
# TYPE shinobi_disk_used_used_space_time_lapse_frames gauge
shinobi_disk_used_used_space_time_lapse_frames {instance="shinobi_test"} 3546.5180377960205
```

### Monitor status

*Monitors status of each camera when changeds*

Event name: **monitor_status**

Metric name: **monitor_status**

*Additional labels*
| Event              | Description                                    |
|--------------------|------------------------------------------------|
| id                 | Monitor ID                                     |
| ke                 | Group ID                                       |

```ini
# HELP shinobi_monitor_status_code Monitor status code
# TYPE shinobi_monitor_status_code gauge
shinobi_monitor_status_code{id="id",ke="ke",instance="shinobi_test"} 2
```

Status Mapping:
| Status            | Description                                     |
|-------------------|-------------------------------------------------|
| 0                 | Disabled                                        |
| 1                 | Starting                                        |
| 2                 | Watching                                        |
| 3                 | Recording                                       |
| 4                 | Restarting                                      |
| 5                 | Stopped                                         |
| 6                 | Idle                                            |
| 7                 | Died                                            |
| 8                 | Stopping                                        |
| 9                 | Started                                         |

### Viewers count

*Monitors viewers counter*

Event name: **viewer_count**

Metric name: **viewer_count**

*Additional labels*
| Event              | Description                                    |
|--------------------|------------------------------------------------|
| id                 | Monitor ID                                     |
| ke                 | Group ID                                       |

```ini
# HELP shinobi_viewer_count_viewers Monitor viewers counter
# TYPE shinobi_viewer_count_viewers gauge
shinobi_viewer_count_viewers{ke="{Group ID}",id="{Monitor ID}", instance="shinobi_test"} 0
```

### Detector Trigger

*Monitors detected events from all plugins*

Event name: **detector_trigger**

Metric name: **detector_trigger**

*Additional labels*
| Event              | Description                                    |
|--------------------|------------------------------------------------|
| id                 | Monitor ID                                     |
| ke                 | Group ID                                       |
| plug               | Plugin name                                    |
| name               | Object looked for to detect                    |
| reason             | Detection type                                 |

```ini
# HELP shinobi_detector_trigger_confidence Detection confifidence level
# TYPE shinobi_detector_trigger_confidence gauge
shinobi_detector_trigger_confidence {id="{Monitor ID}", ke="{Group ID}", plug="audio", name="db", reason="soundChange", instance="shinobi_test"} 5.11
shinobi_detector_trigger_confidence {id="{Monitor ID}", ke="{Group ID}", plug="built-in", name="multipleRegions", reason="motion", instance="shinobi_test"} 1

# HELP shinobi_detector_trigger_duration Process time in milliseconds
# TYPE shinobi_detector_trigger_duration gauge
shinobi_detector_trigger_duration {id="{Monitor ID}", ke="{Group ID}", plug="audio", name="db", reason="soundChange", instance="shinobi_test"} 0
shinobi_detector_trigger_duration {id="{Monitor ID}", ke="{Group ID}", plug="built-in", name="multipleRegions", reason="motion", instance="shinobi_test"} 0
shinobi_detector_trigger_duration {id="{Monitor ID}", ke="{Group ID}", plug="DeepStack-Face", name="{Monitor ID}", reason="face", instance="shinobi_test"} 86

# HELP shinobi_detector_trigger_count Counter of processing
# TYPE shinobi_detector_trigger_count gauge
shinobi_detector_trigger_count {id="{Monitor ID}", ke="{Group ID}", plug="audio", name="db", reason="soundChange", instance="shinobi_test"} 3503
shinobi_detector_trigger_count {id="{Monitor ID}", ke="{Group ID}", plug="built-in", name="multipleRegions", reason="motion", instance="shinobi_test"} 1569
shinobi_detector_trigger_count {id="{Monitor ID}", ke="{Group ID}", plug="DeepStack-Face", name="{Monitor ID}", reason="face", instance="shinobi_test"} 21
```

### Detector Trigger (Prdeiction)

*Extension of detctor trigger, per prediction when supported by the plugin (plugin publishes details.matrices)*

Event name: **detector_trigger**

Metric name: **detector_trigger_prediction**

*Additional labels*
| Event              | Description                                    |
|--------------------|------------------------------------------------|
| id                 | Monitor ID                                     |
| ke                 | Group ID                                       |
| plug               | Plugin name                                    |
| name               | Object looked for to detect                    |
| reason             | Detection type                                 |
| tag                | Object identified by the plugin                |
| person*            | Name of the person identified                  |

*person is relevant for face recognition only by supported plugins (Currently DeepStack and CodeProject AI only)*

```ini
# HELP shinobi_detector_trigger_prediction_count Counter of processing
# TYPE shinobi_detector_trigger_prediction_count gauge
shinobi_detector_trigger_prediction_count {id="{Monitor ID}", ke="{Group ID}", plug="DeepStack-Face", name="{Monitor ID}", reason="face", tag="unknown", person="null", instance="shinobi_test"} 1
shinobi_detector_trigger_prediction_count {id="{Monitor ID}", ke="{Group ID}", plug="DeepStack-Face", name="{Monitor ID}", reason="face", tag="{Face Image}", person="{Face Name}",instance="shinobi_test"} 1

# HELP shinobi_detector_trigger_prediction_confidence Predicition confifidence level
# TYPE shinobi_detector_trigger_prediction_confidence gauge
shinobi_detector_trigger_prediction_confidence {id="{Monitor ID}", ke="{Group ID}", plug="DeepStack-Face", name="{Monitor ID}", reason="face", tag="unknown", person="null", instance="shinobi_test"} 0
shinobi_detector_trigger_prediction_confidence {id="{Monitor ID}", ke="{Group ID}", plug="DeepStack-Face", name="{Monitor ID}", reason="face", tag="{Face Image}", person="{Face Name}",instance="shinobi_test"} 0.72
```

## Troubleshoting

this module logs errors to Shinobi Video log, when opening an issue, please attach the specific error message you have received to allow me to provide you quick response.

### Errors and their potential reasons

#### Invalid configuration for {metricName}.{key}

No configuration of metrics found for the specific key in the metric's configuration

#### Missing description for {metricName}.{key}

No description defined for the specific key in the metric's configuration

#### Failed to publish metric for {metricName}, Key: {key}, Value: {value}, Labels: {labels}, Error: {error}

Metric is defined incorrectly

#### Failed to parse metric {eventName}, Data: ${data}

Metric is not defined
