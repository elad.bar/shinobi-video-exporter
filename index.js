const client = require("prom-client");

const EVENT_MONITOR_STATUS = "monitor_status";
const EVENT_DISK_USED = "diskUsed";
const EVENT_VIEWER_COUNT = "viewer_count";
const EVENT_DETECTOR_TRIGGER = "detector_trigger";
const EVENT_OS = "os";

const METRIC_DISK_USED = "disk_used";
const METRIC_DETECTOR_TRIGGER_PREDICTION = `${EVENT_DETECTOR_TRIGGER}_prediction`;

const FIELD_CONFIDENCE = "confidence";
const FIELD_COUNT = "count";
const FIELD_DURATION = "duration";
const FIELD_TIME = "time";

const METRIC_CONFIG_FIELD_BUCKETS = "buckets";

const DURATION_FIELDS = [FIELD_TIME, FIELD_DURATION];

const MOMENTRY_METRICS = ["confidence", "duration"];

const DEFAULT_INSTANCE_NAME = "shinobi";
const ENDPOINT_METRICS = "metrics";
const ENDPOINT_GRAFANA_DASHBOARD = "metrics/grafana";
const HEADER_CONTENT_TYPE = "Content-Type";

const METRIC_TYPE_GAUGE = "gauge";
const METRIC_TYPE_HISTOGRAM = "histogram";

const HISTOGRAM_BUCKETS = {
};

class ShinobiExporter {
	instance = null;
	eventHandlers = null;
	metricInstances = null;
	metricValueHandlers = null;
	apiPrefix = null;
	app = null;
	clientRegistry = null;
	metricsConfiguration = null;
	grafanaDashboard = null;
	histogramKeys = null;
	metricInitializers = null;
	metricValueModifers = null;

	constructor(shinobiConfig, app, s) {
		this.app = app;
		
		this.instance = DEFAULT_INSTANCE_NAME;
		this.eventHandlers = {};
		this.metricInstances = {};
		this.metricValueHandlers = {};
		this.clientRegistry = null;
		this.metricsConfiguration = null;
		this.apiPrefix = null;
		this.grafanaDashboard = null;
		this.histogramKeys = null;
		this.metricInitializers = null;
		this.metricHandlers = null;
		this.momentryMetrics = null;
		
		this.loadMetricHandlers();
		this.loadConfiguration(shinobiConfig);

		s.onProcessReadyExtensions.push((isReady) => this.onProcessReady(isReady));
		s.onWebsocketMessageSendExtensions.push((d, xn, tx) => this.onWebsocketMessage(d, xn, tx));
	}

	loadMomentryMetrics() {
		this.momentryMetrics = [];
		const metrics = this.metricsConfiguration.metrics;
		const metricNames = Object.keys(metrics);

		metricNames.forEach(metricName => {
			const metricMetadata = metrics[metricName];

			const metricItems = metricMetadata.metrics;
			const metricItemKeys = Object.keys(metricItems);

			metricItemKeys.filter(k => MOMENTRY_METRICS.includes(k))
							.map(k => this.getMetricID(metricName, k))
						  	.forEach(k => this.momentryMetrics.push(k))


		});
	}

	cleanMomentryMetrics() {
		this.momentryMetrics.forEach(k => {
			const metricInstance = this.metricInstances[k];

			if(!!metricInstance) {
				metricInstance.reset();
			}
		})
	}

	loadMetricHandlers() {
		this.histogramKeys = Object.keys(HISTOGRAM_BUCKETS);
		
		this.metricInitializers = {};
		this.metricInitializers[METRIC_TYPE_GAUGE] = (data) => new client.Gauge(data);
		this.metricInitializers[METRIC_TYPE_HISTOGRAM] = (data) => new client.Histogram(data);

		this.metricValueModifers = {}
		this.metricValueModifers[METRIC_TYPE_GAUGE] = (metricInstance, metricName, key, value, labels) => this.setGaugeValue(metricInstance, metricName, key, value, labels);
		this.metricValueModifers[METRIC_TYPE_HISTOGRAM] = (metricInstance, metricName, key, value, labels) => this.setHistogramValue(metricInstance, metricName, key, value, labels);
	}

	getMetricConfiguration(metricName) {
		const metricConfiguration = this.metricsConfiguration.metrics[metricName];

		return metricConfiguration;
	}

	loadModuleConfig() {
		try {
			this.metricsConfiguration = require("./metrics.json");
			this.grafanaDashboard = require("./grafana.json");

			const moduleConfig = require("./conf.json");

			this.instance = moduleConfig.instance;
		} catch { }
	};

	addEventHandler(key, handler) {
		this.eventHandlers[key] = handler;
	};

	addMetricValueHandlers(key, handler) {
		this.metricValueHandlers[key] = handler;
	};

	loadConfiguration(shinobiConfig) {
		this.loadModuleConfig();
		this.loadMomentryMetrics();

		this.apiPrefix = shinobiConfig.webPaths.apiPrefix;

		this.clientRegistry = new client.Registry();

		this.addEventHandler(EVENT_MONITOR_STATUS, (d) => this.onMonitorStatusEvent(d));
		this.addEventHandler(EVENT_DISK_USED, (d) => this.onDiskUsageEvent(d));
		this.addEventHandler(EVENT_VIEWER_COUNT, (d) => this.onViewerCountEvent(d));
		this.addEventHandler(EVENT_DETECTOR_TRIGGER, (d) => this.onDetctorTriggerEvent(d));
		this.addEventHandler(EVENT_OS, (d) => this.onOSEvent);

		this.addMetricValueHandlers(FIELD_CONFIDENCE, (data, key) => this.metricValueHandlerFloat(data, key));
	};

	metricValueHandlerFloat(data, key) {
		const currentValue = data[key];

		if (currentValue !== undefined && currentValue !== null) {
			if (typeof currentValue === "string") {
				data[key] = parseFloat(currentValue);
			} else {
				data[key] = currentValue;
			}
		}
	};

	getMetricID(metricName, key) {
		const fullMetricName = `${this.metricsConfiguration.name}_${metricName}_${key}`;

		return fullMetricName;
	};

	getMetricType(key) {
		const metricType = this.histogramKeys.includes(key) ? METRIC_TYPE_HISTOGRAM : METRIC_TYPE_GAUGE;

		return metricType;
	};

	canLoadMetric(metricName, key) {
		const configuration = this.getMetricConfiguration(metricName);
		const metrics = configuration.metrics;
		const errors = [];

		if (metrics === undefined) {
			errors.push(`Invalid configuration for ${metricName}.${key}`);
		} else {
			const metricConfigurationDescription = metrics[key];

			if (metricConfigurationDescription === undefined) {
				errors.push(`Missing description for ${metricName}.${key}`);
			}
		}

		const canLoad = errors.length === 0;

		if(!canLoad) {
			const errorMessage = errors.join(", ");

			console.error(errorMessage);
		}

		return canLoad;
	};

	loadMetricInstance(metricName, key) {
		const hasValidConfiguration = this.canLoadMetric(metricName, key);

		if(!hasValidConfiguration) {
			return null;
		}

		const fullMetricName = this.getMetricID(metricName, key);	

		const configuration = this.getMetricConfiguration(metricName);
		const metrics = configuration.metrics;
		const labels = configuration.labels;
		const metricType = this.getMetricType(key);

		const metricConfigurationDescription = metrics[key];

		const configData = {
			name: fullMetricName,
			help: metricConfigurationDescription,
			labelNames: labels,
		};

		if(metricType === METRIC_TYPE_HISTOGRAM) {
			configData[METRIC_CONFIG_FIELD_BUCKETS] = HISTOGRAM_BUCKETS[key];
		}
		
		const merticInitializer = this.metricInitializers[metricType];
		
		const merticInstance = merticInitializer(configData);

		this.metricInstances[fullMetricName] = merticInstance;

		this.clientRegistry.registerMetric(merticInstance);
		
		return merticInstance;
	}

	getMetricInstance(metricName, key) {
		const fullMetricName = this.getMetricID(metricName, key);			

		const existingMetricInstance = this.metricInstances[fullMetricName];

		if(!!existingMetricInstance) {
			return existingMetricInstance;
		}

		const newInstance = this.loadMetricInstance(metricName, key);

		return newInstance;
	};

	setGaugeValue(metricInstance, metricName, key, value, labels) {
		if (key === FIELD_COUNT && metricName === EVENT_DETECTOR_TRIGGER) {
			metricInstance.inc(labels);
		} else {
			metricInstance.set(labels, value);
		}
	}

	setHistogramValue(metricInstance, metricName, key, value, labels) {
		metricInstance.observe(labels, value);
	}

	getMetricValueModifer(key) {
		const metricType = this.getMetricType(key);

		const metricValueModifer = this.metricValueModifers[metricType];

		return metricValueModifer;
	}

	publishMetric(metricName, key, value, labels) {
		try {
			const metricInstance = this.getMetricInstance(metricName, key);

			const metricValueModifer = this.getMetricValueModifer(key);

			metricValueModifer(metricInstance, metricName, key, value, labels);
		} catch (error) {
			console.error(
				`Failed to publish metric for '${metricName}', Key: ${key}, Value: ${value}, Labels: ${JSON.stringify(labels)}, Error: ${error}`
			);
		}
	};

	publishMetricData(metricName, metricLabels, data) {
		const metricDataKeys = Object.keys(data);
		const labels = {
			instance: this.instance
		};

		metricDataKeys
			.filter((k) => metricLabels.includes(k))
			.forEach((k) => {
				labels[k] = data[k];
			});

		metricDataKeys
			.filter((k) => !metricLabels.includes(k))
			.forEach((k) => {
				this.publishMetric(metricName, k, data[k], labels);
			});
	};

	publishData(data) {
		const metricName = data.f;
		const metricConfig = this.metricsConfiguration.metrics[metricName];

		if (metricConfig === undefined) {
			console.error(`Failed to parse metric '${metricName}', Data: ${JSON.stringify(data)}`);

		} else {
			const clonedData = this.getCloned(data);
			delete clonedData.f;

			const metricValueHandlerKeys = Object.keys(this.metricValueHandlers);
			const dataKeys = Object.keys(clonedData);

			dataKeys.filter((k) => metricValueHandlerKeys.includes(k))
					.forEach((k) => {
						const handler = this.metricValueHandlers[k];

						handler(clonedData, k);
					});

			this.publishMetricData(metricName, metricConfig.labels, clonedData);
		}
	};

	onMonitorStatusEvent(d) {
		delete d.status;

		this.publishData(d);
	};

	onDiskUsageEvent(d) {
		const data = {
			f: METRIC_DISK_USED,
			limit: d.limit,
			size: d.size,
			used_space: d.usedSpace,
			used_space_videos: d.usedSpaceVideos,
			used_space_file_bin: d.usedSpaceFilebin,
			used_space_time_lapse_frames: d.usedSpaceTimelapseFrames,
		};

		this.publishData(data);
	};

	onViewerCountEvent(d) {
		this.publishData(d);
	};

	getCloned(data) {
		const cloneData = JSON.parse(JSON.stringify(data));

		return cloneData;
	};

	handleDetctorTriggerPredictions(data, prediction) {
		const metricConfiguration = this.getMetricConfiguration(METRIC_DETECTOR_TRIGGER_PREDICTION);
		const canExtend = metricConfiguration.supportedReasons.includes(data.reason);

		if(canExtend) {
			const clonedData = this.getCloned(data);
			clonedData.f = METRIC_DETECTOR_TRIGGER_PREDICTION;

			delete clonedData.duration;
			
			const fields = metricConfiguration.supportedFields;
			const predictionKeys = Object.keys(prediction);
			
			fields.filter((f) => predictionKeys.includes(f))
					.forEach((f) => {
						clonedData[f] = prediction[f];
					});

			this.publishData(clonedData);
		}
	};

	getDuration(details) {
		const durationParts = DURATION_FIELDS.map((k) => details[k]);
		durationParts.push(0);

		const cleanedDurations = durationParts.filter((d) => d !== undefined && d !== null);
		const duration = cleanedDurations[0];

		return duration;
	};

	onDetctorTriggerEvent(d) {
		const details = d.details;
		const duration = this.getDuration(details);
		const predictions = details.matrices;

		const data = {
			f: d.f,
			id: d.id,
			ke: d.ke,
			plug: details.plug,
			name: details.name,
			reason: details.reason,
			confidence: details.confidence,
			duration: duration,
			count: 1,
		};

		if(data.confidence === undefined) {
			const confidenceLevels = predictions.map((m) => m.confidence);
			const maxConfidenceLevel = Math.max(...confidenceLevels);

			data.confidence = maxConfidenceLevel;
		}
		
		this.publishData(data);

		if (!!predictions) {
			predictions.forEach((m) => this.handleDetctorTriggerPredictions(data, m));
		}
	};

	onOSEvent(d) {
		const data = {
			f: d.f,
			cpu: d.cpu,
			ram_used: d.ram.used,
			ram_used_percent: d.ram.percent,
		};

		this.publishData(data);
	};

	onProcessReady(_isReady) {
		this.registerGetMetricsEndpoint();
	};

	onWebsocketMessage(d, _cn, _tx) {
		const handler = this.eventHandlers[d.f];

		if (handler !== undefined) {
			const clonedData = this.getCloned(d);

			handler(clonedData);
		} 
	};

	registerGetMetricsEndpoint() {
		const metricsEndpoint = `${this.apiPrefix}${ENDPOINT_METRICS}`;
		const grafanaDashboardEndpoint = `${this.apiPrefix}${ENDPOINT_GRAFANA_DASHBOARD}`;

		this.app.get(grafanaDashboardEndpoint, async (req, res) => {
			res.send(this.grafanaDashboard);
		});

		this.app.get(metricsEndpoint, async (req, res) => {
			const clientRegistry = this.clientRegistry;
			const lastMetrics = await clientRegistry.metrics();

			res.setHeader(HEADER_CONTENT_TYPE, clientRegistry.contentType);
			res.end(lastMetrics);

			setTimeout(() => this.cleanMomentryMetrics(), 100);
		});
	};
}

module.exports = (s, shinobiConfig, lang, app, io) => new ShinobiExporter(shinobiConfig, app, s);
